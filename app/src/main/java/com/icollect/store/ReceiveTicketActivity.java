package com.icollect.store;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.icollect.store.api.HttpRequest;
import com.icollect.store.api.RSA;
import com.icollect.store.helpers.NetworkDetector;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;


public class ReceiveTicketActivity extends AppCompatActivity
{
    String mSession;
    SharedPreferences sp;
    LinearLayout qrdecoderviewContainer;
    private QRCodeReaderView qrcodeSannerView;
    RSAPublicKey publicKey;
    boolean onceDetected;
    private BluetoothAdapter bluetoothAdapter;
    private boolean receiverIsRegistered = false;
    private BluetoothSocket bluetoothSocket;
    String MACAddress;
    UUID uuid;
    private BluetoothDevice clientDevice;
    private InputStream instream;
    private OutputStream outstream;
    private static final int MESSAGE_READ = 2;
    String message;
    boolean storeHasNetwork;

    @Override
    protected void onResume()
    {
        if (qrcodeSannerView != null) {
            qrcodeSannerView.getCameraManager().startPreview();
        }
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        if (qrcodeSannerView != null) {
            qrcodeSannerView.getCameraManager().stopPreview();
        }
        super.onPause();
    }

    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            switch (msg.what) {
                case MESSAGE_READ:
                    Bundle b = msg.getData();
                    boolean isTicketValid = b.getBoolean("is_ticket_valid");
                    int tid = b.getInt("tid");
                    String ticketContent = b.getString("ticket_content");
                    showTicketMessageDialog(isTicketValid, tid, ticketContent);
                    break;
            }
        }
    };

    public void showTicketMessageDialog(boolean isTicketValid, final int tid, String ticketContent)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReceiveTicketActivity.this);
        if (isTicketValid) {
            try {
                if (storeHasNetwork && NetworkDetector.getInstance().isNetworkAvailable(ReceiveTicketActivity.this)) {
                    Boolean isDisable = (Boolean) new DisableTicketAsyncTask().execute(tid).get();
                    JSONObject response = new JSONObject();
                    response.put("status", isDisable);
                    response.put("message", message);
                    if (isDisable) {
                        response.put("tid", tid);
                        //using Bluetooth
                        if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
                            outstream.write(response.toString().getBytes());
                            bluetoothSocket.close();
                        }
                        alertDialog.setTitle("核銷成功").setMessage("票卷資料吻合，消費成功\n" + "票券編號 : " + tid);
                        alertDialog.setPositiveButton("確定", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                onceDetected = false;
                                if (qrcodeSannerView != null) {
                                    ((ViewGroup) qrcodeSannerView.getParent()).removeView(qrcodeSannerView);
                                }
                                qrdecoderviewContainer = (LinearLayout) findViewById(R.id.qrdecoderview_container);
                                qrcodeSannerView = new QRCodeReaderView(getApplicationContext());
                                qrcodeSannerView.setOnQRCodeReadListener(new QRBluetoothOnQRCodeReadListener());
                                qrdecoderviewContainer.addView(qrcodeSannerView);
                                QRBluetoothOnQRCodeReadListener qrBluetoothOnQRCodeReadListener = new QRBluetoothOnQRCodeReadListener();
                                qrcodeSannerView.setOnQRCodeReadListener(qrBluetoothOnQRCodeReadListener);
                                qrcodeSannerView.getCameraManager().startPreview();
                                dialog.dismiss();
                            }
                        }).create();
                    } else {
                        //using Bluetooth
                        if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
                            try {
                                bluetoothSocket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        alertDialog.setTitle("核銷失敗").setMessage("票卷核銷失敗，請檢查網路狀況\n" + message);
                        alertDialog.setPositiveButton("確定", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                onceDetected = false;
                                if (qrcodeSannerView != null) {
                                    ((ViewGroup) qrcodeSannerView.getParent()).removeView(qrcodeSannerView);
                                }
                                qrdecoderviewContainer = (LinearLayout) findViewById(R.id.qrdecoderview_container);
                                qrcodeSannerView = new QRCodeReaderView(getApplicationContext());
                                qrcodeSannerView.setOnQRCodeReadListener(new QRBluetoothOnQRCodeReadListener());
                                qrdecoderviewContainer.addView(qrcodeSannerView);
                                QRBluetoothOnQRCodeReadListener qrBluetoothOnQRCodeReadListener = new QRBluetoothOnQRCodeReadListener();
                                qrcodeSannerView.setOnQRCodeReadListener(qrBluetoothOnQRCodeReadListener);
                                qrcodeSannerView.getCameraManager().startPreview();
                                dialog.dismiss();
                            }
                        }).create();
                    }
                } else {
                    alertDialog = offlineVerification(tid);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            alertDialog.setTitle("驗證失敗").setMessage("票卷簽章與資料不合\n" + ticketContent);
            alertDialog.setPositiveButton("取消", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    JSONObject response = new JSONObject();
                    try {
                        response.put("status", false);
                        response.put("message", "驗證失敗，票卷簽章與資料不合");

                        if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
                            outstream.write(response.toString().getBytes());
                            bluetoothSocket.close();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            }).create();
        }
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private AlertDialog.Builder offlineVerification(int tid) throws JSONException, IOException
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReceiveTicketActivity.this);
        boolean isValid = true;
        String blackListJsonArrayString = sp.getString("blackListTidArray", null);
        JSONArray blackListTidArray = null;
        if (blackListJsonArrayString != null) {
            blackListTidArray = new JSONArray(blackListJsonArrayString);
            for (int i = 0; i < blackListTidArray.length(); i++) {
                int usedTid = blackListTidArray.getInt(i);
                if (tid == usedTid) {
                    isValid = false;
                }
            }
        }

        JSONObject response = new JSONObject();
        String message = null;
        if (isValid) {
            response.put("tid", tid);
            message = "票券合法，核銷成功";

            if (blackListTidArray == null) {
                blackListTidArray = new JSONArray();
            }
            blackListTidArray.put(tid);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("blackListTidArray", blackListTidArray.toString());
            editor.commit();

            alertDialog.setTitle("核銷成功").setMessage("票卷資料吻合，消費成功\n" + "票券編號 : " + tid);
            alertDialog.setPositiveButton("確定", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    onceDetected = false;
                    if (qrcodeSannerView != null) {
                        ((ViewGroup) qrcodeSannerView.getParent()).removeView(qrcodeSannerView);
                    }
                    qrdecoderviewContainer = (LinearLayout) findViewById(R.id.qrdecoderview_container);
                    qrcodeSannerView = new QRCodeReaderView(getApplicationContext());
                    qrcodeSannerView.setOnQRCodeReadListener(new QRBluetoothOnQRCodeReadListener());
                    qrdecoderviewContainer.addView(qrcodeSannerView);
                    QRBluetoothOnQRCodeReadListener qrBluetoothOnQRCodeReadListener = new QRBluetoothOnQRCodeReadListener();
                    qrcodeSannerView.setOnQRCodeReadListener(qrBluetoothOnQRCodeReadListener);
                    qrcodeSannerView.getCameraManager().startPreview();
                    dialog.dismiss();
                }
            }).create();
        } else {
            message = "票券已被使用";

            alertDialog.setTitle("核銷失敗").setMessage("票卷已被使用\n" + "票券編號 : " + tid);
            alertDialog.setPositiveButton("確定", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    onceDetected = false;
                    if (qrcodeSannerView != null) {
                        ((ViewGroup) qrcodeSannerView.getParent()).removeView(qrcodeSannerView);
                    }
                    qrdecoderviewContainer = (LinearLayout) findViewById(R.id.qrdecoderview_container);
                    qrcodeSannerView = new QRCodeReaderView(getApplicationContext());
                    qrcodeSannerView.setOnQRCodeReadListener(new QRBluetoothOnQRCodeReadListener());
                    qrdecoderviewContainer.addView(qrcodeSannerView);
                    QRBluetoothOnQRCodeReadListener qrBluetoothOnQRCodeReadListener = new QRBluetoothOnQRCodeReadListener();
                    qrcodeSannerView.setOnQRCodeReadListener(qrBluetoothOnQRCodeReadListener);
                    qrcodeSannerView.getCameraManager().startPreview();
                    dialog.dismiss();
                }
            }).create();
        }
        response.put("message", message);
        response.put("status", isValid);
        if (bluetoothSocket != null && bluetoothSocket.isConnected()) {
            outstream.write(response.toString().getBytes());
            bluetoothSocket.close();
        }
        return alertDialog;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        if (bluetoothSocket != null) {
                            try {
                                bluetoothSocket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        bluetoothAdapter.enable();
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        setUpQRCodeReader();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_bluetooth_collect);

        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);
        storeHasNetwork = sp.getBoolean("hasNetwork", false);

        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        String modulusBase64 = sp.getString("modulus", null);
        String exponentBase64 = sp.getString("exponent", null);

        if ((modulusBase64 != null) && (exponentBase64 != null)) {
            BigInteger modulus = new BigInteger(Base64.decode(modulusBase64, Base64.DEFAULT));
            BigInteger exponent = new BigInteger(Base64.decode(exponentBase64, Base64.DEFAULT));
            publicKey = RSA.getPublicKey(modulus, exponent);
        } else {
            finish();
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            Toast.makeText(this, "不支援藍芽，只能核銷發送至Email的票券", Toast.LENGTH_SHORT);
        }

        if (!bluetoothAdapter.isEnabled()) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            registerReceiver(mReceiver, filter);
            receiverIsRegistered = true;
            bluetoothAdapter.enable();
        } else {
            setUpQRCodeReader();
        }
    }

    private void setUpQRCodeReader()
    {
        qrdecoderviewContainer = (LinearLayout) findViewById(R.id.qrdecoderview_container);
        qrcodeSannerView = new QRCodeReaderView(getApplicationContext());
        qrcodeSannerView.setOnQRCodeReadListener(new QRBluetoothOnQRCodeReadListener());
        qrdecoderviewContainer.addView(qrcodeSannerView);
        QRBluetoothOnQRCodeReadListener qrBluetoothOnQRCodeReadListener = new QRBluetoothOnQRCodeReadListener();
        qrcodeSannerView.setOnQRCodeReadListener(qrBluetoothOnQRCodeReadListener);
        qrcodeSannerView.getCameraManager().startPreview();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (receiverIsRegistered) {
            unregisterReceiver(mReceiver);
        }

        if (bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
        }
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    class QRBluetoothOnQRCodeReadListener implements QRCodeReaderView.OnQRCodeReadListener
    {
        @Override
        public void onQRCodeRead(String s, PointF[] pointFs)
        {
            if (!onceDetected) {
                onceDetected = true;
                try {
                    JSONObject o = new JSONObject(s);
                    if (o.has("uuid")) {
                        String address = o.getString("address");
                        String uuidString = o.getString("uuid");
                        if (bluetoothAdapter.isEnabled() && BluetoothAdapter.checkBluetoothAddress(address)) {
                            MACAddress = address;
                            uuid = UUID.fromString(uuidString);
                            new ConnectThread().start();
                        }
                    } else {
                        qrcodeSannerView.getCameraManager().stopPreview();
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReceiveTicketActivity.this);
                        alertDialog.setTitle("訊息").setMessage("請使用消費者App出示QRCode");
                        alertDialog.setPositiveButton("確定", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                                finish();
                            }
                        }).create();
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void cameraNotFound()
        {

        }

        @Override
        public void QRCodeNotFoundOnCamImage()
        {

        }
    }

    class DisableTicketAsyncTask extends AsyncTask<Integer, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Integer... params)
        {
            int tid = params[0];
            Map<String, String> data = new HashMap<>();
            data.put("id", String.valueOf(tid));
            HttpRequest request = HttpRequest.put(getString(R.string.disable_ticket)).header("Cookie", mSession).form(data);
            if (request.ok()) {
                String response = request.body();
                System.out.println(response);
                try {
                    JSONObject o = new JSONObject(response);
                    Boolean status = o.getBoolean("status");
                    message = o.getString("message");
                    return status;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }
    }

    private class ConnectThread extends Thread
    {
        public ConnectThread()
        {
            clientDevice = bluetoothAdapter.getRemoteDevice(MACAddress);
        }

        public void run()
        {
            qrcodeSannerView.getCameraManager().stopPreview();
            int bufferSize = 10240;
            byte[] buffer = new byte[bufferSize];
            int bytesSize = -1;
            try {
                if (bluetoothAdapter.isEnabled()) {
                    bluetoothSocket = clientDevice.createInsecureRfcommSocketToServiceRecord(uuid);
                    if (bluetoothSocket != null) {
                        bluetoothSocket.connect();
                        if (bluetoothSocket.isConnected()) {
                            instream = bluetoothSocket.getInputStream();
                            outstream = bluetoothSocket.getOutputStream();

                            String ticketMessage = null;
                            bytesSize = instream.read(buffer);
                            ticketMessage = new String(buffer, "UTF-8");
                            JSONObject o = new JSONObject(ticketMessage);
                            String data = o.getString("data");
                            String sign = o.getString("sign");
                            System.out.println(data);
                            System.out.println(sign);
                            boolean ticketValid = RSA.verify(data, Base64.decode(sign, Base64.DEFAULT), publicKey);
                            JSONObject dataJson = new JSONObject(data);
                            int tid = dataJson.getInt("id");

                            Message m = new Message();
                            Bundle b = new Bundle();
                            b.putBoolean("is_ticket_valid", ticketValid);
                            b.putInt("tid", tid);
                            b.putString("ticket_content", o.toString());
                            m.what = MESSAGE_READ;
                            m.setData(b);
                            mHandler.sendMessage(m);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    bluetoothSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void cancel()
        {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void write(byte[] bytes)
        {
            try {
                outstream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class VerifyEmailQRCodeThread extends Thread
    {
        JSONObject emailQRCodeContent;
        public VerifyEmailQRCodeThread(JSONObject emailQRCodeContent)
        {
            this.emailQRCodeContent = emailQRCodeContent;
        }
        @Override
        public void run()
        {
            qrcodeSannerView.getCameraManager().stopPreview();
            try {
                String data = emailQRCodeContent.getString("data");
                String sign = emailQRCodeContent.getString("sign");
                System.out.println(data);
                System.out.println(sign);
                boolean ticketValid = RSA.verify(data, Base64.decode(sign, Base64.DEFAULT), publicKey);
                JSONObject dataJson = new JSONObject(data);
                int tid = dataJson.getInt("id");
                Message m = new Message();
                Bundle b = new Bundle();
                b.putBoolean("is_ticket_valid", ticketValid);
                b.putInt("tid", tid);
                b.putString("ticket_content", emailQRCodeContent.toString());
                m.what = MESSAGE_READ;
                m.setData(b);
                mHandler.sendMessage(m);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
