package com.icollect.store.context;

import android.content.Context;

/**
 * Created by Administrator on 2015/8/19.
 */
public class ContextProvider
{
    Context mainContext;
    private static ContextProvider contextProvider = new ContextProvider();

    private ContextProvider()
    {

    }

    public static ContextProvider getInstance()
    {
        return contextProvider;
    }

    public void setMainContext(Context mainContext)
    {
        this.mainContext = mainContext;
    }

    public Context getManContext()
    {
        return mainContext;
    }
}
