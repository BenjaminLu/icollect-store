package com.icollect.store.entity;

import java.util.List;

/**
 * Created by Administrator on 2015/7/12.
 */
public class ActivityDescription
{
    private int id;
    private String activityImageUrl;
    private String activityName;
    private String activityContent;
    private String gainStartDate;
    private String gainEndDate;
    private String exchangeStartDate;
    private String exchangeEndDate;
    private List<GainRule> gainRules;
    private List<ExchangeRule> exchangeRules;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public List<ExchangeRule> getExchangeRules()
    {
        return exchangeRules;
    }

    public void setExchangeRules(List<ExchangeRule> exchangeRules)
    {
        this.exchangeRules = exchangeRules;
    }

    public List<GainRule> getGainRules()
    {
        return gainRules;
    }

    public void setGainRules(List<GainRule> gainRules)
    {
        this.gainRules = gainRules;
    }

    public String getExchangeEndDate()
    {
        return exchangeEndDate;
    }

    public void setExchangeEndDate(String exchangeEndDate)
    {
        this.exchangeEndDate = exchangeEndDate;
    }

    public String getExchangeStartDate()
    {
        return exchangeStartDate;
    }

    public void setExchangeStartDate(String exchangeStartDate)
    {
        this.exchangeStartDate = exchangeStartDate;
    }

    public String getGainEndDate()
    {
        return gainEndDate;
    }

    public void setGainEndDate(String gainEndDate)
    {
        this.gainEndDate = gainEndDate;
    }

    public String getGainStartDate()
    {
        return gainStartDate;
    }

    public void setGainStartDate(String gainStartDate)
    {
        this.gainStartDate = gainStartDate;
    }

    public String getActivityContent()
    {
        return activityContent;
    }

    public void setActivityContent(String activityContent)
    {
        this.activityContent = activityContent;
    }

    public String getActivityName()
    {
        return activityName;
    }

    public void setActivityName(String activityName)
    {
        this.activityName = activityName;
    }

    public String getActivityImageUrl()
    {
        return activityImageUrl;
    }

    public void setActivityImageUrl(String activityImageUrl)
    {
        this.activityImageUrl = activityImageUrl;
    }
}
