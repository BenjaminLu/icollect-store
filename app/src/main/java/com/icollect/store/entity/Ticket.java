package com.icollect.store.entity;

public class Ticket implements Cloneable
{
    int id;
    String name;
    String body;
    int price;
    String image;
    int activityID;
    int userID;
    boolean status;
    String createAt;
    String updateAt;
    String data;
    String sign;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public int getActivityID()
    {
        return activityID;
    }

    public void setActivityID(int activityID)
    {
        this.activityID = activityID;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public boolean getStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public String getCreateAt()
    {
        return createAt;
    }

    public void setCreateAt(String createAt)
    {
        this.createAt = createAt;
    }

    public String getUpdateAt()
    {
        return updateAt;
    }

    public void setUpdateAt(String updateAt)
    {
        this.updateAt = updateAt;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getSign()
    {
        return sign;
    }

    public void setSign(String sign)
    {
        this.sign = sign;
    }

    public Ticket clone()
    {
        Ticket t = new Ticket();
        t.setId(new Integer(this.getId()));
        t.setName(new String(this.getName()));
        t.setBody(new String(this.getBody()));
        t.setPrice(new Integer(this.getPrice()));
        t.setImage(new String(this.getImage()));
        t.setActivityID(new Integer(this.getActivityID()));
        t.setUserID(new Integer(this.getUserID()));
        t.setStatus(new Boolean(this.getStatus()));
        t.setCreateAt(new String(this.getCreateAt()));
        t.setUpdateAt(new String(this.getUpdateAt()));
        t.setData(new String(this.getData()));
        t.setSign(new String(this.getSign()));
        return t;
    }
}
