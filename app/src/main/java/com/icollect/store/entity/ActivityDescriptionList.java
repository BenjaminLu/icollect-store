package com.icollect.store.entity;

import java.util.List;

/**
 * Created by Administrator on 2015/9/2.
 */
public class ActivityDescriptionList
{
    List<ActivityDescription> activityDescriptions;

    public List<ActivityDescription> getActivityDescriptions()
    {
        return activityDescriptions;
    }

    public void setActivityDescriptions(List<ActivityDescription> activityDescriptions)
    {
        this.activityDescriptions = activityDescriptions;
    }
}
