package com.icollect.store.entity;

/**
 * Created by Administrator on 2015/8/21.
 */
public class ExchangeRule
{
    private int id;
    private String name;
    private String body;
    private Integer threshold;
    private String imageUrl;
    private Integer ticketNumber = 0;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Integer getTicketNumber()
    {
        return ticketNumber;
    }

    public void setTicketNumber(Integer ticketNumber)
    {
        this.ticketNumber = ticketNumber;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getThreshold()
    {
        return threshold;
    }

    public void setThreshold(Integer threshold)
    {
        this.threshold = threshold;
    }
}
