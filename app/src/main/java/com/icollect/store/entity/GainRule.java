package com.icollect.store.entity;

/**
 * Created by Administrator on 2015/8/21.
 */
public class GainRule
{
    private String body;
    private Integer value;
    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public Integer getValue()
    {
        return value;
    }

    public void setValue(Integer value)
    {
        this.value = value;
    }
}
