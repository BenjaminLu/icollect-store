package com.icollect.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.icollect.store.api.HttpRequest;
import com.icollect.store.helpers.NetworkDetector;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity
{
    private TextView title;
    private TextView subtitle;
    private EditText storeIdEditText;
    private EditText passwordEditText;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        storeIdEditText = (EditText) findViewById(R.id.store_id);
        passwordEditText = (EditText) findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String id = storeIdEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                if (!id.equals("") && !password.equals("")) {
                    loginBtn.setEnabled(false);
                    if (NetworkDetector.getInstance().isNetworkAvailable(LoginActivity.this)) {
                        new LoginAsyncTask().execute(id, password);
                    } else {
                        loginBtn.setEnabled(true);
                        SnackbarManager.show(
                                Snackbar.with(getApplicationContext())
                                        .type(SnackbarType.SINGLE_LINE)
                                        .position(Snackbar.SnackbarPosition.BOTTOM)
                                        .text("沒有網路連線，請確認網路暢通")
                                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                        .animation(true)
                                , LoginActivity.this);
                    }
                }
            }
        });

        Animation titleAnimation = AnimationUtils.loadAnimation(this, R.anim.login_title_anim);
        Animation subtitleAnimation = AnimationUtils.loadAnimation(this, R.anim.login_subtitle_anim);
        Animation controlAnimation = AnimationUtils.loadAnimation(this, R.anim.loggin_control_anim);
        title.startAnimation(titleAnimation);
        subtitle.startAnimation(subtitleAnimation);
        storeIdEditText.setAnimation(controlAnimation);
        passwordEditText.setAnimation(controlAnimation);
        loginBtn.setAnimation(controlAnimation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class LoginAsyncTask extends AsyncTask<String, Void, Boolean>
    {
        private String sessionCookie;
        private String message;
        private boolean hasNetwork = false;

        @Override
        protected Boolean doInBackground(String... params)
        {
            String id = params[0];
            String password = params[1];
            Map<String, String> data = new HashMap<String, String>();
            data.put("store_id", id);
            data.put("password", password);
            HttpRequest request = HttpRequest.post(getString(R.string.login_url)).form(data);
            if (request.ok()) {
                String response = request.body();
                System.out.println(response);
                this.sessionCookie = request.header("Set-Cookie");
                try {
                    JSONObject responseJson = new JSONObject(response);
                    Boolean isSuccessful = responseJson.getBoolean("success");
                    if (!isSuccessful) {
                        message = "登入失敗，帳號或密碼錯誤";
                    } else {
                        hasNetwork = responseJson.getBoolean("has_network");
                    }
                    return isSuccessful;
                } catch (JSONException e) {
                    e.printStackTrace();
                    message = "伺服器回應失敗，請聯絡iCollect客服中心";
                    return false;
                }
            }
            message = "伺服器連線無回應，請稍後再試";
            return false;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful) {
                SharedPreferences sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("session", sessionCookie);
                editor.putBoolean("hasNetwork", hasNetwork);
                editor.commit();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            loginBtn.setEnabled(true);
        }
    }
}
