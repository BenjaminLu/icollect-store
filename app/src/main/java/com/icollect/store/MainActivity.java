package com.icollect.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.icollect.store.api.HttpRequest;
import com.icollect.store.context.ContextProvider;
import com.icollect.store.fragment.AllActivitiesFragment;
import com.icollect.store.helpers.NetworkDetector;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
{
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    SharedPreferences sp;
    String mSession;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("activity create");
        ContextProvider.getInstance().setMainContext(getApplicationContext());
        //setup universal image loader
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.image_loading)
                .resetViewBeforeLoading(false)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .build();
        File cacheDir = StorageUtils.getCacheDirectory(getApplicationContext());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .taskExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                .taskExecutorForCachedImages(AsyncTask.THREAD_POOL_EXECUTOR)
                .threadPoolSize(6)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LRULimitedMemoryCache(100 * 1024 * 1024))
                .diskCache(new UnlimitedDiskCache(cacheDir))
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .imageDownloader(new BaseImageDownloader(getApplicationContext()))
                .defaultDisplayImageOptions(options)
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);

        sp = getApplication().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        actionBarDrawerToggle.syncState();
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        LinearLayout syncMenuBtn = (LinearLayout) findViewById(R.id.sync_menu_btn);
        LinearLayout takeTicketBtn = (LinearLayout) findViewById(R.id.take_ticket_btn);
        View.OnClickListener drawerMenuOnClickListener = new DrawerMenuOnClickListener();
        syncMenuBtn.setOnClickListener(drawerMenuOnClickListener);
        takeTicketBtn.setOnClickListener(drawerMenuOnClickListener);


        boolean isNetworkAvailable = NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this);
        Bundle b = new Bundle();
        b.putBoolean("isNetworkAvailable", isNetworkAvailable);

        if (isNetworkAvailable) {
            if (mSession != null) {
                new GetPublicKeyAsyncTask().execute();
                new LoginAsyncTask().execute(mSession);
            } else {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }

        fragmentManager = getSupportFragmentManager();
        Fragment allActivityFragment = new AllActivitiesFragment();
        allActivityFragment.setArguments(b);
        fragmentManager.beginTransaction().add(R.id.all_activity_fragment_container, allActivityFragment).commit();
    }

    @Override
    protected void onDestroy()
    {
        System.out.println("activity destroy");
        super.onDestroy();
    }

    @Override
    protected void onPause()
    {
        System.out.println("activity pause");
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        System.out.println("activity resume");
        super.onResume();
    }

    @Override
    protected void onStop()
    {
        System.out.println("activity stop");
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            if (NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this)) {
                new LogoutAsyncTask().execute();
            } else {
                SnackbarManager.show(
                        Snackbar.with(getApplicationContext())
                                .type(SnackbarType.SINGLE_LINE)
                                .position(Snackbar.SnackbarPosition.BOTTOM)
                                .text("沒有網路連線，請確認網路暢通")
                                .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                .animation(true)
                        , MainActivity.this);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class DrawerMenuOnClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId()) {
                case R.id.sync_menu_btn:
                    String blackListJsonArrayString = sp.getString("blackListTidArray", null);
                    System.out.println(blackListJsonArrayString);
                    if (blackListJsonArrayString != null) {
                        try {
                            JSONArray blackListTidArray = new JSONArray(blackListJsonArrayString);
                            if (blackListTidArray.length() > 0) {
                                if (NetworkDetector.getInstance().isNetworkAvailable(MainActivity.this)) {
                                    boolean isSuccessful = new LoginAsyncTask().execute(mSession).get();
                                    new BulkDisableTicketAsyncTask().execute(blackListTidArray);
                                } else {
                                    Toast.makeText(getApplicationContext(), "沒有網路連線，請檢查網路狀況後再試", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "沒有需要同步的已核銷票券", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "沒有需要同步的已核銷票券", Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.take_ticket_btn:
                    Intent takeTicketIntent = new Intent(MainActivity.this, ReceiveTicketActivity.class);
                    startActivity(takeTicketIntent);
                    break;
            }
        }
    }

    class BulkDisableTicketAsyncTask extends AsyncTask<JSONArray, Void, Void>
    {
        JSONArray ticketReports;
        @Override
        protected Void doInBackground(JSONArray... params)
        {
            JSONArray blackListTidArray = params[0];
            Map<String, String> data = new HashMap<>();
            data.put("tids", blackListTidArray.toString());
            HttpRequest request = HttpRequest.put(getString(R.string.bulk_disable_ticket)).header("Cookie", mSession).form(data);
            if (request.ok()) {
                String response = request.body();
                System.out.println(response);
                try {
                    ticketReports = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                String response = request.body();
                System.out.println(response);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg)
        {
            if (ticketReports != null && ticketReports.length() > 0) {
                for (int i = 0 ; i < ticketReports.length(); i++) {
                    try {
                        JSONObject ticketReport = ticketReports.getJSONObject(i);
                        int tid = ticketReport.getInt("tid");
                        boolean status = ticketReport.getBoolean("status");
                        String message = ticketReport.getString("message");
                        if (status) {
                            Toast.makeText(getApplicationContext(), "核銷成功! 票券編號 : " + tid + " \n" +
                                    message + "\n"
                                    , Toast.LENGTH_LONG).show();
                        } else {
                            String userName = ticketReport.getString("user_name");
                            String userEmail = ticketReport.getString("user_email");
                            Toast.makeText(getApplicationContext(), "核銷失敗! 票券編號 : " + tid + "\n" +
                                    message + "\n" +
                                    "消費者姓名" + userName + "\n" +
                                    "消費者信箱" + userEmail, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                sp.edit().putString("blackListTidArray", null).commit();
            }
        }
    }

    class LoginAsyncTask extends AsyncTask<String, Void, Boolean>
    {
        JSONObject responseJson;
        String sessionCookie;

        @Override
        protected Boolean doInBackground(String... params)
        {
            boolean isSuccessful = false;
            String session = params[0];

            Map<String, String> data = new HashMap<String, String>();
            data.put("store_id", "anything");
            data.put("password", "anything");
            HttpRequest request = HttpRequest.post(getString(R.string.login_url)).header("Cookie", session).form(data);
            if (request.ok()) {
                String response = request.body();
                System.out.println(response);
                try {
                    responseJson = new JSONObject(response);
                    isSuccessful = responseJson.getBoolean("success");
                    sessionCookie = request.header("Set-Cookie");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return isSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful.booleanValue()) {
                SharedPreferences.Editor editor = sp.edit();
                if (!sessionCookie.equals(mSession)) {
                    editor.putString("session", sessionCookie);
                    editor.commit();
                }
            } else {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }
    }

    class LogoutAsyncTask extends AsyncTask<Void, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... params)
        {
            boolean isSuccessful = false;
            HttpRequest request = HttpRequest.get(getString(R.string.logout_url));
            if (request.ok()) {
                String response = request.body();
                JSONObject o;
                try {
                    o = new JSONObject(response);
                    isSuccessful = o.getBoolean("success");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return isSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean isSuccessful)
        {
            if (isSuccessful.booleanValue()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.remove("session").commit();
                //Logout AsyncTask
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            } else {
                SnackbarManager.show(Snackbar.with(getApplicationContext())
                        .type(SnackbarType.SINGLE_LINE)
                        .text("登出失敗，伺服器連線異常")
                        .duration(Snackbar.SnackbarDuration.LENGTH_LONG)
                        .animation(true), MainActivity.this);
            }
        }
    }

    class GetPublicKeyAsyncTask extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected Void doInBackground(Void... params)
        {
            HttpRequest request = HttpRequest.get(getString(R.string.get_public_key));
            if (request.ok()) {
                String response = request.body();
                try {
                    JSONObject o = new JSONObject(response);
                    String modulusBase64 = o.getString("modulus");
                    String exponentBase64 = o.getString("exponent");
                    sp.edit().putString("modulus", modulusBase64)
                            .putString("exponent", exponentBase64)
                            .commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
