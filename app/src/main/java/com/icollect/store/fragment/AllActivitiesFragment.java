package com.icollect.store.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.icollect.store.R;
import com.icollect.store.SellTicketActivity;
import com.icollect.store.adapter.AllFragmentRecyclerViewAdapter;
import com.icollect.store.api.HttpRequest;
import com.icollect.store.entity.ActivityDescription;
import com.icollect.store.entity.ActivityDescriptionList;
import com.icollect.store.entity.ExchangeRule;
import com.icollect.store.itemanimator.CustomItemAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AllActivitiesFragment extends Fragment
{
    private String allActivityOfGroupApiUrl;
    private String icollectDomain;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    AllFragmentRecyclerViewAdapter allFragmentRecycleViewAdapter;
    SharedPreferences sp;
    String mSession;
    View layout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle b = getArguments();
        boolean isNetworkAvailable = b.getBoolean("isNetworkAvailable");
        layout = inflater.inflate(R.layout.fragment_all_activity, container, false);
        sp = getActivity().getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);
        String activityDescriptionListJson = sp.getString("activityDescriptionList", null);
        ActivityDescriptionList activityDescriptionList = null;
        if (activityDescriptionListJson != null) {
            activityDescriptionList = new Gson().fromJson(activityDescriptionListJson, ActivityDescriptionList.class);
        }

        allActivityOfGroupApiUrl = getString(R.string.group_activity_all_api);
        icollectDomain = getString(R.string.icollect_domain);
        progressBar = (ProgressBar) layout.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) layout.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new CustomItemAnimator());
        allFragmentRecycleViewAdapter = new AllFragmentRecyclerViewAdapter(new ArrayList<ActivityDescription>(), R.layout.partial_fragment_all_activity_row, getActivity());
        recyclerView.setAdapter(allFragmentRecycleViewAdapter);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (isNetworkAvailable) {
            new ShowAllActivityTask().execute();
        } else {
            if (activityDescriptionList != null) {
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                allFragmentRecycleViewAdapter.clearActivities();
                allFragmentRecycleViewAdapter.addActivities(activityDescriptionList.getActivityDescriptions());
            }
        }
        return layout;
    }

    @Override
    public void onDestroyView()
    {
        System.out.println("destroy fragment view");
        allFragmentRecycleViewAdapter.clearActivities();
        super.onDestroyView();
    }

    @Override
    public void onDestroy()
    {
        System.out.println("fragment destroy");
        super.onDestroy();
    }

    private class ShowAllActivityTask extends AsyncTask<Void, Void, Void>
    {
        List<ActivityDescription> activityDescriptions = new ArrayList<>();

        @Override
        protected Void doInBackground(Void... params)
        {
            HttpRequest request = HttpRequest.get(allActivityOfGroupApiUrl).header("Cookie", mSession);
            if (request.ok()) {
                String response = request.body();
                System.out.println(response);
                try {
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        int id = object.getInt("id");
                        String name = object.getString("name");
                        String body = object.getString("body");
                        String imageUrl = icollectDomain + "/" + object.getString("image");
                        String gainStartDate = object.getString("gain_start");
                        String gainEndDate = object.getString("gain_end");
                        String exchangeStartDate = object.getString("ex_start");
                        String exchangeEndDate = object.getString("ex_end");

                        JSONArray exchangeRulesJson = null;
                        ArrayList<ExchangeRule> exchangeRules = null;
                        if (object.has("exRules")) {
                            exchangeRulesJson = object.getJSONArray("exRules");
                            exchangeRules = new ArrayList<>();
                            for (int position = 0; position < exchangeRulesJson.length(); position++) {
                                JSONObject exchangeRuleJson = exchangeRulesJson.getJSONObject(position);
                                int exchangeRuleId = exchangeRuleJson.getInt("id");
                                String exchangeRuleName = exchangeRuleJson.getString("name");
                                String exchangeRuleBody = exchangeRuleJson.getString("body");
                                Integer exchangeRuleThreshold = exchangeRuleJson.getInt("threshold");
                                String exchangeRuleImageUrl = exchangeRuleJson.getString("image");
                                exchangeRuleImageUrl = getString(R.string.icollect_domain) + "/" + exchangeRuleImageUrl;
                                ImageLoader.getInstance().loadImageSync(exchangeRuleImageUrl);
                                ExchangeRule exchangeRule = new ExchangeRule();
                                exchangeRule.setId(exchangeRuleId);
                                exchangeRule.setName(exchangeRuleName);
                                exchangeRule.setBody(exchangeRuleBody);
                                exchangeRule.setThreshold(exchangeRuleThreshold);
                                exchangeRule.setImageUrl(exchangeRuleImageUrl);
                                exchangeRules.add(exchangeRule);
                            }
                        }

                        ActivityDescription activityDescription = new ActivityDescription();
                        activityDescription.setId(id);
                        activityDescription.setActivityName(name);
                        activityDescription.setActivityContent(body);
                        activityDescription.setActivityImageUrl(imageUrl);
                        activityDescription.setGainStartDate(gainStartDate);
                        activityDescription.setGainEndDate(gainEndDate);
                        activityDescription.setExchangeStartDate(exchangeStartDate);
                        activityDescription.setExchangeEndDate(exchangeEndDate);
                        activityDescription.setExchangeRules(exchangeRules);
                        activityDescriptions.add(activityDescription);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            ActivityDescriptionList activityDescriptionList = new ActivityDescriptionList();
            activityDescriptionList.setActivityDescriptions(activityDescriptions);
            sp.edit().putString("activityDescriptionList", new Gson().toJson(activityDescriptionList)).commit();

            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            allFragmentRecycleViewAdapter.clearActivities();
            allFragmentRecycleViewAdapter.addActivities(activityDescriptions);
            super.onPostExecute(result);
        }
    }
}
