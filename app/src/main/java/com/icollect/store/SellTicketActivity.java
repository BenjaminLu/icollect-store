package com.icollect.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.icollect.store.adapter.SellExchangeRulesRecyclerViewAdapter;
import com.icollect.store.api.HttpRequest;
import com.icollect.store.entity.ActivityDescription;
import com.icollect.store.entity.ExchangeRule;
import com.icollect.store.helpers.NetworkDetector;
import com.icollect.store.itemanimator.CustomItemAnimator;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SellTicketActivity extends AppCompatActivity
{
    ActivityDescription activityDescription;
    List<ExchangeRule> exchangeRules;
    RecyclerView exchangeRulesRecyclerView;
    SellExchangeRulesRecyclerViewAdapter sellExchangeRulesRecyclerViewAdapter;
    FloatingActionButton fab;
    Toolbar toolbar;
    EditText customerEmailEditText;
    String email;
    SharedPreferences sp;
    String mSession;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_ticket);
        sp = getSharedPreferences(getString(R.string.login_shared_preferences_key), Context.MODE_PRIVATE);
        mSession = sp.getString("session", null);
        Bundle extras = getIntent().getExtras();
        activityDescription = new Gson().fromJson(extras.getString("activity"), ActivityDescription.class);
        exchangeRules = activityDescription.getExchangeRules();

        exchangeRulesRecyclerView = (RecyclerView) findViewById(R.id.sell_exchange_rule_list);
        sellExchangeRulesRecyclerViewAdapter = new SellExchangeRulesRecyclerViewAdapter(new ArrayList<ExchangeRule>(), R.layout.partial_activities_sell_exchange_rule_row, this);
        exchangeRulesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        exchangeRulesRecyclerView.setItemAnimator(new CustomItemAnimator());
        exchangeRulesRecyclerView.setAdapter(sellExchangeRulesRecyclerViewAdapter);
        sellExchangeRulesRecyclerViewAdapter.addExchangeRules(exchangeRules);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        customerEmailEditText = (EditText) findViewById(R.id.customer_email);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                List<ExchangeRule> exchangeRules = sellExchangeRulesRecyclerViewAdapter.getExchangeRules();
                if (NetworkDetector.getInstance().isNetworkAvailable(SellTicketActivity.this)) {
                    if (!customerEmailEditText.getText().toString().equals("")) {
                        fab.setEnabled(false);
                        email = customerEmailEditText.getText().toString();
                        new SellTicketFromStoreAsyncTask().execute(exchangeRules);
                    } else {
                        SnackbarManager.show(com.nispok.snackbar.Snackbar.with(getApplicationContext())
                                .type(SnackbarType.SINGLE_LINE)
                                .text("請輸入客戶的信箱")
                                .duration(com.nispok.snackbar.Snackbar.SnackbarDuration.LENGTH_LONG)
                                .animation(true), SellTicketActivity.this);
                    }
                } else {
                    SnackbarManager.show(com.nispok.snackbar.Snackbar.with(getApplicationContext())
                            .type(SnackbarType.SINGLE_LINE)
                            .text("沒有網路連線，請檢查網路連線狀況")
                            .duration(com.nispok.snackbar.Snackbar.SnackbarDuration.LENGTH_LONG)
                            .animation(true), SellTicketActivity.this);
                }
            }
        });
    }

    class SellTicketFromStoreAsyncTask extends AsyncTask<List<ExchangeRule>, Void, Void>
    {
        boolean status;
        String message;
        @Override
        protected Void doInBackground(List<ExchangeRule>... lists)
        {
            List<ExchangeRule> exchangeRules = lists[0];
            JSONArray requestTickets = new JSONArray();
            for (ExchangeRule rule : exchangeRules) {
                JSONObject o = new JSONObject();
                try {
                    o.put("ex_rule_id", rule.getId());
                    o.put("number", rule.getTicketNumber());
                    requestTickets.put(o);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONObject request = new JSONObject();
            try {
                request.put("email", email);
                request.put("tickets", requestTickets);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HttpRequest sellRequest = HttpRequest.post(getString(R.string.sell_ticket_from_store)).header("Cookie", mSession).contentType("application/json").send(request.toString());

            if (sellRequest.ok()) {
                String response = sellRequest.body();
                System.out.println(response);
                try {
                    JSONObject o = new JSONObject(response);
                    status = o.getBoolean("status");
                    message = o.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                String response = sellRequest.body();
                System.out.println(response);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if (status) {
                Toast.makeText(SellTicketActivity.this, message, Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(SellTicketActivity.this, "購買失敗 : " + message, Toast.LENGTH_LONG).show();
                fab.setEnabled(true);
            }
        }
    }

}
