package com.icollect.store.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icollect.store.DetailActivity;
import com.icollect.store.R;
import com.icollect.store.entity.Ticket;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class DetailTicketsRecyclerViewAdapter
        extends RecyclerView.Adapter<DetailTicketsRecyclerViewAdapter.ViewHolder>
{
    private List<Ticket> tickets;
    private int activityOfTicketsRowLayout;
    private DetailActivity activity;

    public DetailTicketsRecyclerViewAdapter(ArrayList<Ticket> tickets, int activityOfTicketsRowLayout, DetailActivity a)
    {
        this.tickets = tickets;
        this.activityOfTicketsRowLayout = activityOfTicketsRowLayout;
        this.activity = a;
    }

    public void clearTickets()
    {
        if (tickets != null) {
            int size = this.tickets.size();
            tickets.clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addTickets(ArrayList<Ticket> newTickets)
    {
        if (tickets != null) {
            int lastSize = tickets.size();
            this.tickets.addAll(newTickets);
            this.notifyItemRangeInserted(lastSize, newTickets.size());
        }
    }

    public static ArrayList<Ticket> cloneTickets(List<Ticket> list)
    {
        ArrayList<Ticket> clone = new ArrayList<Ticket>(list.size());
        for (Ticket ticket : list) {
            clone.add(ticket.clone());
        }
        return clone;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(activityOfTicketsRowLayout, viewGroup, false);
        System.out.println(allActivityRowLayoutView.getId());
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        final Ticket ticket = tickets.get(i);
        ImageLoader.getInstance().displayImage(ticket.getImage(), viewHolder.image);

        viewHolder.ticketName.setText(ticket.getName());
        viewHolder.ticketBody.setText(ticket.getBody());
        viewHolder.ticketPrice.setText(String.valueOf(ticket.getPrice()) + "元");
    }

    @Override
    public int getItemCount()
    {
        return tickets == null ? 0 : tickets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView image;
        public TextView ticketName;
        public TextView ticketBody;
        public TextView ticketPrice;

        public ViewHolder(View row)
        {
            super(row);
            image = (ImageView) row.findViewById(R.id.ticket_image);
            ticketName = (TextView) row.findViewById(R.id.ticket_name);
            ticketBody = (TextView) row.findViewById(R.id.ticket_body);
            ticketPrice = (TextView) row.findViewById(R.id.ticket_price);
        }
    }
}
