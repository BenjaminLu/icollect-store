package com.icollect.store.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icollect.store.DetailActivity;
import com.icollect.store.R;
import com.icollect.store.entity.Store;

import java.util.ArrayList;
import java.util.List;

public class DetailStoresRecyclerViewAdapter
        extends RecyclerView.Adapter<DetailStoresRecyclerViewAdapter.ViewHolder>
{
    private List<Store> stores;
    private int activityOfStoresRowLayout;
    private DetailActivity activity;

    public DetailStoresRecyclerViewAdapter(ArrayList<Store> tickets, int activityOfTicketsRowLayout, DetailActivity a)
    {
        this.stores = tickets;
        this.activityOfStoresRowLayout = activityOfTicketsRowLayout;
        this.activity = a;
    }

    public void clearStores()
    {
        if (stores != null) {
            int size = this.stores.size();
            stores.clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addStores(ArrayList<Store> newTickets)
    {
        if (stores != null) {
            int lastSize = stores.size();
            this.stores.addAll(newTickets);
            this.notifyItemRangeInserted(lastSize, newTickets.size());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(activityOfStoresRowLayout, viewGroup, false);
        System.out.println(allActivityRowLayoutView.getId());
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        final Store ticket = stores.get(i);
        final String address = ticket.getAddress();
        viewHolder.address.setText(address);
        viewHolder.address.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + address));
                i.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return stores == null ? 0 : stores.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView address;

        public ViewHolder(View row)
        {
            super(row);
            address = (TextView) row.findViewById(R.id.detail_store_address);
        }
    }
}
