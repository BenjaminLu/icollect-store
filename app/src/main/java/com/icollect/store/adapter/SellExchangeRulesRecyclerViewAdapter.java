package com.icollect.store.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.icollect.store.R;
import com.icollect.store.SellTicketActivity;
import com.icollect.store.entity.ExchangeRule;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;
import java.util.List;

public class SellExchangeRulesRecyclerViewAdapter extends RecyclerView.Adapter<SellExchangeRulesRecyclerViewAdapter.ViewHolder>
{
    private List<ExchangeRule> exchangeRules;
    private int activityOfSellTicketRowLayout;
    private SellTicketActivity activity;

    public SellExchangeRulesRecyclerViewAdapter(ArrayList<ExchangeRule> exchangeRules, int activityOfSellTicketRowLayout, SellTicketActivity a)
    {
        this.exchangeRules = exchangeRules;
        this.activityOfSellTicketRowLayout = activityOfSellTicketRowLayout;
        this.activity = a;
    }

    public void clearExchangeRules()
    {
        if (exchangeRules != null) {
            int size = this.exchangeRules.size();
            exchangeRules.clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addExchangeRules(List<ExchangeRule> newTickets)
    {
        if (exchangeRules != null) {
            int lastSize = exchangeRules.size();
            this.exchangeRules.addAll(newTickets);
            this.notifyItemRangeInserted(lastSize, newTickets.size());
        }
    }

    public List<ExchangeRule> getExchangeRules()
    {
        return this.exchangeRules;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(activityOfSellTicketRowLayout, viewGroup, false);
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i)
    {
        final ExchangeRule exchangeRule = exchangeRules.get(i);
        ImageLoader.getInstance().displayImage(exchangeRule.getImageUrl(), viewHolder.exchangeRuleImage);
        viewHolder.exchangeRuleName.setText(exchangeRule.getName());
        viewHolder.exchangeRuleThreshold.setText(String.valueOf(exchangeRule.getThreshold()) + "元");
        viewHolder.ticketNumber.setProgress(exchangeRule.getTicketNumber());
        viewHolder.ticketNumber.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener()
        {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser)
            {
                exchangeRule.setTicketNumber(value);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar)
            {

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return exchangeRules == null ? 0 : exchangeRules.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView exchangeRuleImage;
        public TextView exchangeRuleName;
        public DiscreteSeekBar ticketNumber;
        public TextView exchangeRuleThreshold;

        public ViewHolder(View row)
        {
            super(row);
            exchangeRuleImage = (ImageView) row.findViewById(R.id.exchange_rule_image);
            exchangeRuleName = (TextView) row.findViewById(R.id.exchange_rule_name);
            ticketNumber = (DiscreteSeekBar) row.findViewById(R.id.ticket_number);
            exchangeRuleThreshold = (TextView) row.findViewById(R.id.sell_ticket_exchange_rule_price);
        }
    }
}
