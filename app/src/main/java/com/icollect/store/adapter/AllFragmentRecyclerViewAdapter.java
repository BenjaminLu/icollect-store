package com.icollect.store.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.icollect.store.DetailActivity;
import com.icollect.store.R;
import com.icollect.store.SellTicketActivity;
import com.icollect.store.entity.ActivityDescription;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class AllFragmentRecyclerViewAdapter
        extends RecyclerView.Adapter<AllFragmentRecyclerViewAdapter.ViewHolder>
{
    private List<ActivityDescription> activities;
    private int allActivityRowLayout;
    private Context mainActivity;

    public AllFragmentRecyclerViewAdapter(List<ActivityDescription> activities, int allActivityRowLayout, Context mainActivity)
    {
        this.activities = activities;
        this.allActivityRowLayout = allActivityRowLayout;
        this.mainActivity = mainActivity;
    }

    public void clearActivities()
    {
        int size = this.activities.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                activities.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addActivities(List<ActivityDescription> applications)
    {
        int lastSize = activities.size();
        this.activities.addAll(applications);
        this.notifyItemRangeInserted(lastSize, applications.size() - 1);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int i)
    {
        View allActivityRowLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(allActivityRowLayout, viewGroup, false);
        return new ViewHolder(allActivityRowLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i)
    {
        final ActivityDescription activityDescription = activities.get(i);
        viewHolder.activityName.setText(activityDescription.getActivityName());
        viewHolder.gainStartDateTextView.setText(activityDescription.getGainStartDate());
        ImageLoader.getInstance().displayImage(activityDescription.getActivityImageUrl(), viewHolder.image);

        viewHolder.activityBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(mainActivity, DetailActivity.class);
                Bundle b = new Bundle();
                b.putString("activity", new Gson().toJson(activityDescription));
                i.putExtras(b);
                mainActivity.startActivity(i);
            }
        });
        viewHolder.sellBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(mainActivity, SellTicketActivity.class);
                Bundle b = new Bundle();
                b.putString("activity", new Gson().toJson(activityDescription));
                i.putExtras(b);
                mainActivity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return activities == null ? 0 : activities.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView activityName;
        public ImageView image;
        public LinearLayout activityBtn;
        public TextView sellBtn;
        public TextView gainStartDateTextView;

        public ViewHolder(View allActivityRowLayoutView)
        {
            super(allActivityRowLayoutView);
            activityName = (TextView) allActivityRowLayoutView.findViewById(R.id.activityName);
            image = (ImageView) allActivityRowLayoutView.findViewById(R.id.banner_image);
            activityBtn = (LinearLayout) allActivityRowLayoutView.findViewById(R.id.activityBtn);
            sellBtn = (TextView) allActivityRowLayoutView.findViewById(R.id.sell_btn);
            gainStartDateTextView = (TextView) allActivityRowLayoutView.findViewById(R.id.activityStartDate);
        }

    }
}
