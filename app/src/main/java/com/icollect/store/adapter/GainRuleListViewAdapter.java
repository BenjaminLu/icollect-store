package com.icollect.store.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.icollect.store.R;
import com.icollect.store.entity.GainRule;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/9/3.
 */
public class GainRuleListViewAdapter extends BaseAdapter
{
    private Context context;
    private LayoutInflater layoutInflator;
    private List<GainRule> gainRules;
    private List<Boolean> checkStates;

    static class ViewHolder
    {
        TextView gainRuleBody;
        TextView gainRuleValue;
        CheckBox gainRuleCheckBox;
    }

    public GainRuleListViewAdapter(Context c, List<GainRule> gainRules)
    {
        this.gainRules = gainRules;
        checkStates = new ArrayList<>();
        for (int i = 0; i < gainRules.size(); i++) {
            checkStates.add(false);
        }
        this.context = c;
        this.layoutInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return gainRules.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflator.inflate(R.layout.partial_qrcollect_dialog_listview_row, null);
            holder = new ViewHolder();
            holder.gainRuleBody = (TextView) convertView.findViewById(R.id.gain_rule_body);
            holder.gainRuleValue = (TextView) convertView.findViewById(R.id.gain_rule_value);
            holder.gainRuleCheckBox = (CheckBox) convertView.findViewById(R.id.gain_rule_check_box);
            holder.gainRuleCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    checkStates.set(position, isChecked);
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.gainRuleBody.setText(gainRules.get(position).getBody());
        holder.gainRuleValue.setText(gainRules.get(position).getValue().toString());
        holder.gainRuleCheckBox.setChecked(checkStates.get(position));

        return convertView;
    }

    public Integer getTotalCheckedPoint()
    {
        Integer total = 0;
        for (int i = 0; i < gainRules.size(); i++) {
            GainRule gainRule = gainRules.get(i);
            if (checkStates.get(i)) {
                total += gainRule.getValue();
            }
        }

        return total;
    }
}
