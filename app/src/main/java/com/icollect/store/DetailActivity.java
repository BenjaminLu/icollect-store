package com.icollect.store;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.icollect.store.adapter.DetailStoresRecyclerViewAdapter;
import com.icollect.store.adapter.DetailTicketsRecyclerViewAdapter;
import com.icollect.store.api.HttpRequest;
import com.icollect.store.entity.ActivityDescription;
import com.icollect.store.entity.ExchangeRule;
import com.icollect.store.entity.Store;
import com.icollect.store.entity.Ticket;
import com.icollect.store.helpers.NetworkDetector;
import com.icollect.store.itemanimator.CustomItemAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity
{
    ImageView bannerParallaxImage;
    ActivityDescription activityDescription;
    RecyclerView ticketList;
    RecyclerView storeList;
    DetailTicketsRecyclerViewAdapter ticketsRecyclerViewAdapter;
    DetailStoresRecyclerViewAdapter storesRecyclerViewAdapter;
    DisplayMetrics displayMetrics;
    TextView ticketsHeader;
    TextView storesHeader;
    TextView gainStartDate;
    TextView gainEndDate;
    TextView exStartDate;
    TextView exEndDate;
    List<ExchangeRule> exchangeRules;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle extras = getIntent().getExtras();
        activityDescription = new Gson().fromJson(extras.getString("activity"), ActivityDescription.class);
        exchangeRules = activityDescription.getExchangeRules();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        TextView activityBodyTextView = (TextView) findViewById(R.id.activity_body_textview);
        collapsingToolbar.setTitle(activityDescription.getActivityName());
        bannerParallaxImage = (ImageView) findViewById(R.id.banner_parallax_image);
        ImageLoader.getInstance().displayImage(activityDescription.getActivityImageUrl(), bannerParallaxImage);
        setSupportActionBar(toolbar);

        activityBodyTextView.setText(activityDescription.getActivityContent());

        ticketsHeader = (TextView) findViewById(R.id.tickets_header);
        storesHeader = (TextView) findViewById(R.id.stores_header);

        ticketsHeader.setVisibility(View.GONE);
        storesHeader.setVisibility(View.GONE);

        gainStartDate = (TextView) findViewById(R.id.gain_start_date);
        gainEndDate = (TextView) findViewById(R.id.gain_end_date);
        exStartDate = (TextView) findViewById(R.id.ex_start_date);
        exEndDate = (TextView) findViewById(R.id.ex_end_date);

        gainStartDate.setText(activityDescription.getGainStartDate());
        gainEndDate.setText(activityDescription.getGainEndDate());
        exStartDate.setText(activityDescription.getExchangeStartDate());
        exEndDate.setText(activityDescription.getExchangeEndDate());

        ticketList = (RecyclerView) findViewById(R.id.detail_ticket_list);
        storeList = (RecyclerView) findViewById(R.id.detail_store_list);

        ticketsRecyclerViewAdapter = new DetailTicketsRecyclerViewAdapter(new ArrayList<Ticket>(), R.layout.partial_activities_detail_tickets_row, this);
        storesRecyclerViewAdapter = new DetailStoresRecyclerViewAdapter(new ArrayList<Store>(), R.layout.partial_activities_detail_stores_row, this);

        ticketList.setAdapter(ticketsRecyclerViewAdapter);
        ticketList.setLayoutManager(new LinearLayoutManager(this));
        ticketList.setItemAnimator(new CustomItemAnimator());
        ticketList.setHasFixedSize(false);
        ticketList.setNestedScrollingEnabled(false);

        storeList.setAdapter(storesRecyclerViewAdapter);
        storeList.setLayoutManager(new LinearLayoutManager(this));
        storeList.setItemAnimator(new CustomItemAnimator());
        storeList.setHasFixedSize(false);
        storeList.setNestedScrollingEnabled(false);

        displayMetrics = getResources().getDisplayMetrics();

        if (NetworkDetector.getInstance().isNetworkAvailable(DetailActivity.this)) {

        }

        //new GetTicketsOfActivityAsyncTask().execute(activityDescription.getId());
        //new GetStoresOfActivityAsyncTask().execute(activityDescription.getId());

        if (exchangeRules != null) {
            ArrayList<Ticket> tickets = new ArrayList<>();
            for (int i = 0 ; i < exchangeRules.size(); i++) {
                ExchangeRule exchangeRule = exchangeRules.get(i);
                Ticket ticket = new Ticket();
                ticket.setName(exchangeRule.getName());
                ticket.setBody(exchangeRule.getBody());
                ticket.setPrice(exchangeRule.getThreshold());
                ticket.setImage(exchangeRule.getImageUrl());
                tickets.add(ticket);
            }

            ticketsRecyclerViewAdapter.addTickets(tickets);
            float dpHeight = 205 * displayMetrics.density;
            ticketList.setMinimumHeight((int) (tickets.size() * dpHeight));

            if (tickets.size() > 0) {
                ticketsHeader.setVisibility(View.VISIBLE);
            }
        }
    }
}
